const app = require('./app');

// Separated app.listen for "testing" files to work properly

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
  console.log(`Server started on port: ${PORT}`);
});

/////
