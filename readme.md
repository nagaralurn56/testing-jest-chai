# Express Route - Testing with Jest and Chai

## Install Dependencies

```
npm install
```

## Run App

```
# Run the App
npm run start

# Testing the App
npm run test
```

## Usage

1. Install the required dependencies using 'npm install'.
2. Run the application to start the server on localhost:5000 using 'npm run start'.
3. I used postman to verify my solutions.
4. For the first route, solution can be achieved using http://localhost:5000/api/ping.
5. For the second route, solutions can be achieved using following examples:
   http://localhost:5000/api/posts?tags=science
   http://localhost:5000/api/posts?tags=history&sortBy=id
   http://localhost:5000/api/posts?tags=tech&direction=desc&sortBy=popularity
6. Run the tests using 'npm run test'

- Version: 1.0.0
- License: MIT

### Author: Ramesh Nagaralu
