const express = require('express');
const axios = require('axios');

const app = express();

app.use(express.json({ extended: false }));

// Simple route: Get method
app.get('/api/posts', async (req, res) => {
  // read query params, validate and Error handling
  if (!req.query.tags) return res.status(400).json({ error: 'Tags parameter is required!' });
  let tags = req.query.tags.split(',');

  let sortBy = req.query.sortBy || 'id';
  let regexSort = /\b(id|reads|likes|popularity)\b/;
  if (!regexSort.test(sortBy))
    return res.status(400).json({ error: 'sortBy parameter is invalid!' });

  let direction = req.query.direction || 'asc';
  let regexDir = /\b(asc|desc)\b/;
  if (!regexDir.test(direction))
    return res.status(400).json({ error: 'Direction parameter is invalid!' });

  // concurrent or parallel calls to API using Promise.all
  var promises = [];
  for (let tag of tags) {
    // Removing the API - not sure if I can share or not
    promises.push(axios.get(`some API-not sharing here-replace accordingly?tag=${tag}`));
  }

  try {
    var responses = await Promise.all(promises); // responses will be an array
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Error in getting data - Promise all' });
  }

  // merge all responses
  let responsesArr = [];
  for (let i = 0; i < responses.length; i++) {
    responsesArr = responsesArr.concat(...responses[i].data.posts);
  }

  // remove duplicates using Set, convert back to Array and finally sort
  const results = {};
  const uniq = new Set(responsesArr.map((e) => JSON.stringify(e)));

  if (direction === 'asc') {
    results['posts'] = Array.from(uniq)
      .map((e) => JSON.parse(e))
      .sort(function (a, b) {
        return parseFloat(a[sortBy]) - parseFloat(b[sortBy]);
      });
  } else {
    results['posts'] = Array.from(uniq)
      .map((e) => JSON.parse(e))
      .sort(function (a, b) {
        return parseFloat(b[sortBy]) - parseFloat(a[sortBy]);
      });
  }

  res.status(200).json(results);
});

module.exports = app;
