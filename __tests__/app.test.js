const request = require('supertest');
const axios = require('axios');
const chai = require('chai');
const app = require('../app');
expect = chai.expect;
chai.use(require('chai-sorted'));

// Tests for non existing route:
test('Should return status code 404 for non existing routes', async (done) => {
  await request(app).get('/api/pinged').send().expect(404);
  done();
});

// Tests for Second route:
// 1. error handling - missing 'tag' query params or invalid query params for 'sortBy' or 'direction'
test('Should return error message and status code 400 without Tag Query params for route 2', async (done) => {
  await request(app).get('/api/posts').send().expect(400, { error: 'Tags parameter is required!' });
  done();
});

test('Should return error message and status code 400 for bad sortBy Query params', async (done) => {
  await request(app)
    .get('/api/posts')
    .query({ tags: 'any', sortBy: 'any' })
    .send()
    .expect(400, { error: 'sortBy parameter is invalid!' });
  done();
});

test('Should return error message and status code 400 for bad direction Query params', async (done) => {
  await request(app)
    .get('/api/posts')
    .query({ tags: 'any', direction: 'any' })
    .send()
    .expect(400, { error: 'Direction parameter is invalid!' });
  done();
});

// 2. status code: success; tag query: empty data for non-existent tags
test('Should return empty posts and 200 status code for Tag Query params without data', async (done) => {
  await request(app).get('/api/posts').query({ tags: 'any' }).send().expect(200, { posts: [] });
  done();
});

// 3. status code: success; tag query: existing tags; sortBy: default ('id'); direction: default (asc)
test('Should return 200 status code with correct Tag Query params and by default the data is sorted by id in ascending order', async (done) => {
  const response = await request(app).get('/api/posts').query({ tags: 'tech' }).send().expect(200);
  expect(response.body.posts).to.be.sortedBy('id', { ascending: true });
  done();
});

// 4. status code: success; tag query: existing tags; sortBy: given; direction: given
test('Should return 200 status code with correct Query params and the data is sorted by query params', async (done) => {
  const response = await request(app)
    .get('/api/posts')
    .query({ tags: 'science', sortBy: 'likes', direction: 'desc' })
    .send()
    .expect(200);
  expect(response.body.posts).to.be.sortedBy('likes', { descending: true });
  done();
});

// 5. status code: success; tag query: multiple existing tags; sortBy: given otherwise default; direction: given otherwise default
test('Should return 200 status code with multiple tag Query params and sorted by default or provided query params', async (done) => {
  const response = await request(app)
    .get('/api/posts')
    .query({ tags: 'history, politics, tech', sortBy: 'popularity' })
    .send()
    .expect(200);
  expect(response.body.posts).to.be.sortedBy('popularity', { descending: false });
  done();
});

// 6. status code: success; tag query: multiple existing tags; sortBy: given otherwise default; direction: given otherwise default
//    check for no duplicate data for multiple tag query params
test('Should pass the test if there are no duplicates', async (done) => {
  let posts = [];
  const response = await request(app)
    .get('/api/posts')
    .query({ tags: 'history, politics', sortBy: 'popularity' });

  const historyData = await axios.get(`some API-not sharing here-replace accordingly?tag=history`);
  posts.push(...historyData.data.posts);
  const politicsData = await axios.get(
    `some API-not sharing here-replace accordingly?tag=politics`
  );
  posts.push(...politicsData.data.posts);

  expect(response.body.posts).to.not.eql(posts); // comparing two arrays are not same
  expect(response.body.posts).not.to.have.lengthOf(posts.length); //comparing the length of two arrays not same
  done();
});
